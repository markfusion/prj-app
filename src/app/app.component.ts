import { Component, OnInit } from '@angular/core';
// import * as firebase from 'firebase';
import { initializeApp } from 'firebase';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  // loadedFeature = 'recipe';
  
  // onNavigate(feature: string){
  //   this.loadedFeature = feature;
  // }

  ngOnInit () {
    // firebase.initializeApp({
    initializeApp({  
      apiKey: "AIzaSyAC4UHJDhw13tPHVHz6hmHZyqxKfsnoj7k",
      authDomain: "ng-recipe-book-4248d.firebaseapp.com"
    });
  }
}
