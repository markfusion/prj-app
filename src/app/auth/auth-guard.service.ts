import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanLoad, Route, UrlSegment } from "@angular/router";
import { AuthService } from "./auth.service";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

@Injectable()
export class AuthGuard implements CanActivate, CanLoad {

    constructor(private authService: AuthService,
                private router: Router) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        let isAuthenticated: boolean = this.authService.isAuthenticated();
        if (!isAuthenticated) {
            this.router.navigate(['/signin']);
        }
        return isAuthenticated;
    }

    canLoad(route: Route, segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {
        let isAuthenticated: boolean = this.authService.isAuthenticated();
        if (!isAuthenticated) {
            this.router.navigate(['/signin']);
        }
        return isAuthenticated;
    }
}