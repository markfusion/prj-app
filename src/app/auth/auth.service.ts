import * as firebase from 'firebase';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class AuthService {
    token: string;
    siginError = new Subject<string>();

    constructor(private router: Router) {}

    signupUser(email: string, password: string) {
        firebase.auth().createUserWithEmailAndPassword(email, password)
            .catch(
                error => console.log(error)
            );
    }

    signinUser(email: string, password: string) {
        firebase.auth().signInWithEmailAndPassword(email, password)
            .then(
                response => { 
                    this.router.navigate(['/']);
                    // console.log(response); 
                    firebase.auth().currentUser.getIdToken()
                    .then(
                        (token: string) => {
                            this.token = token;
                            this.siginError.next(null);
                        } 
                    )
                }
            )
            .catch(
                error => { 
                    console.log('Login' + error); 
                    this.siginError.next('Login' + error);
                }
            );
    }

    logout() {
        firebase.auth().signOut();
        this.token = null;
    }

    getToken() {
        // return 
        firebase.auth().currentUser.getIdToken()
        .then(
            (token: string) => {
                this.token = token;
            }
        );
        return this.token;
        // .then(
        //     reponse => console.log(response)
        // );
    }

    isAuthenticated() {
        return this.token != null;
    }

}