import { NgModule } from "@angular/core";
import { HeaderComponent } from "./header/header.component";
import { HomeComponent } from "./home/home.component";
import { SharedModule } from "../shared/shared.module";
import { AppRoutingModule } from "../app-routing.module";
import { ShoppingListService } from "../shopping-list/shopping-list.service";
import { RecipeService } from "../recipes/recipe.service";
import { DataStorageService } from "../shared/data-storage.service";
import { AuthService } from "../auth/auth.service";
import { AuthGuard } from "../auth/auth-guard.service";

@NgModule({
    declarations: [
        HeaderComponent,
        HomeComponent
    ],
    imports: [
        SharedModule,
        AppRoutingModule
    ],
    exports: [
        AppRoutingModule,
        HeaderComponent
    ],
    providers: [
        ShoppingListService, RecipeService, DataStorageService, AuthService
         /*  couldn't move the AuthGuard to the recipes-routing.module providers for lazy loading 
             'cause it's used with the canLoad on the app-routing.module */

         /*  at the end the AuthGuard was moved to recipes-routing.module cause it had to 
             be removed on the canLoad in favor of the preLoadingStrategy see app-routing.module  */
    ]
})
export class CoreModule {

}