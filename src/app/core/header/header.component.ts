import { Component, Output, EventEmitter } from '@angular/core';
import { DataStorageService } from '../../shared/data-storage.service';
import { Response } from '@angular/http';
import { AuthService } from '../../auth/auth.service';
import { Router } from '@angular/router';


@Component( {
   selector: 'app-header',
   templateUrl: './header.component.html'
} )
export class HeaderComponent {
    // @Output() featureSelected = new EventEmitter<string>();

    // onSelect( feature: string) {
    //     this.featureSelected.emit(feature);
    // }
    isNavbarCollapsed: boolean =  true;

    constructor(private dataStorageService: DataStorageService,
                private authService: AuthService,
                private router: Router) {}

    onSaveData() {
        this.dataStorageService.storeRecipes()
        .subscribe(
            (response: Response) => {
                console.log(response);
            }

        );
    }

    onFetchData() {
        this.dataStorageService.getRecipes();
        this.router.navigate(['/recipes'])
    }

    onLogout() {
        this.authService.logout();
        this.router.navigate(['/signin']);
    }

    isAuthenticated() {
        return this.authService.isAuthenticated()
    }

    toggleNavBarCollapse() {
        this.isNavbarCollapsed = !this.isNavbarCollapsed;
    }    
}


